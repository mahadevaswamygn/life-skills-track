# 1. What are the activities you do that make you relax - Calm quadrant?
- I take short breaks to do meditation, which helps me calm down and find peace within myself.
- I like to solve puzzles and play games like chess to give my mind a good workout and keep it sharp.
- I take a few moments of quietness to create a sense of calmness and find inner peace within myself.
- Listening to classical music can create a calming and tranquil ambiance, helping me feel at peace and relaxed.

# 2. When do you find getting into the Stress quadrant?
- When I come across challenging problems in my project that I can't solve, I pause for a moment to regain my focus and inner balance.
- When I don't make enough progress, it can cause feelings of stress and anxiety to arise.
- I sometimes experience conflicts or tensions in my relationships with other people.

# 3. How do you understand if you are in the Excitement quadrant?
- When I'm in the "Excitement" quadrant, I find myself laughing and singing a lot.
- My mind becomes fixated on the exciting activity or event I'm about to engage in, making it difficult to concentrate on anything else.
- I have a positive and hopeful mindset, believing that good things will come my way.

# 4. Paraphrase the Sleep is your Superpower video in detail.
- Sleep is a precious time when my body and brain take a break to rest and rejuvenate.
- Not getting enough sleep can have negative effects on overall health and raise the risk of conditions such as heart attacks and cancers.
- Lack of sufficient sleep can weaken the immune system.
- Not getting enough sleep can disrupt various biological processes in the body
- It is important to prioritize sleep as it contributes to better memory, overall health, and overall well-being.

# 5. What are some ideas that you can implement to sleep better?
- It is beneficial to follow a consistent sleep schedule by going to bed and waking up at the same times each day.
- To promote better sleep, it is helpful to create a comfortable sleep environment in your bedroom. This can be achieved by keeping the room dark, quiet, and cool.
- I will try relaxation techniques, such as deep breathing or progressive muscle relaxation, to help my body and mind relax before sleep.

# 6. Paraphrase the video - Brain Changing Benefits of Exercise. Minimum 5 points.
- Exercise boosts mood, energy, and focus.
- Exercise increases blood flow to the brain, providing nourishment to brain cells and promoting their growth.
- Engaging in physical activity releases chemicals in the brain that boost happiness and reduce stress.
- Regular exercise improves cognitive function, which includes memory and problem-solving skills.
- Regular exercise helps decrease the risk of cognitive decline and age-related brain diseases such as Alzheimer's.

# 7. What are some steps you can take to exercise more?
- Discover enjoyable activities that you eagerly anticipate, making exercise more enjoyable and motivating.
- Establish a consistent exercise schedule and commit to it in order to make physical activity a regular and consistent part of your routine.
- Try to climb stairs at least once a month, aiming for around 500 steps, as part of your regular routine.
- Try to exercise at least once a day, either in the morning or afternoon, for a minimum of 10 to 30 minutes.

