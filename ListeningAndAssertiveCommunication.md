## What are the steps/strategies to do Active Listening?
The act of fully hearing and comprehending the meaning of what someone else is saying is called active listening.

Here some steps:
- To avoid getting disturbed by your own thoughts, it is important to focus on the speaker and the topic at hand.
- Try not to interrupt the other person.
- Let them finish, and then respond.
- Show that you are listening with body language.
- If it's suitable, it's a good idea to take notes during important conversations.
- Paraphrase what the other person has said to ensure that both of you are on the same page.


## According to Fisher's model, what are the key points of Reflective Listening?
Reflective listening is a way of communicating where you try to understand what someone is saying and then repeat it back to them to make sure you understood correctly.

### Two key points:
1. Seeking to understand: It means making an effort to really grasp and comprehend what the speaker is saying. This includes paying attention to their words, tone, and body language to gain a deeper understanding of their message.
2. Offering the idea back: After understanding the speaker's message, Reflective Listening involves paraphrasing or summarizing what they said and expressing it back to them. This step confirms that you have correctly understood their thoughts and shows that you are actively engaged in the conversation.

## What are the obstacles in your listening process?
Here some common challenges I face in the listening process include:
- Getting easily distracted: I find it hard to focus and pay attention to what others are saying when there are distractions around.
- Not fully listening: Sometimes, I realize that I'm not really listening and my mind starts to wander, so I end up missing important things people say.
- Having preconceived ideas: If we already have certain beliefs or opinions, we might filter or misunderstand what the speaker is trying to convey based on our existing thoughts.

## What can you do to improve your listening?
- Give my full attention.
- Avoid interrupting.
- Practice active listening.
- Ask clarifying questions.

## When do you switch to Passive communication style in your day to day life?
When I want to avoid conflicts.

## When do you switch into Aggressive communication styles in your day to day life?
When I get impatient or frustrated, I sometimes use aggression to show my unhappiness or try to take control. I'm aware that it's not a good approach and I'm working on finding better ways to handle my frustrations.

## When do you switch into Passive Aggressive (sarcasm/gossiping/taunts/silent treatment and others) communication styles in your day to day life?
- Unresolved conflicts: When there are unresolved conflicts or underlying tensions with someone, I may resort to passive-aggressive behaviors as a way to indirectly express my anger or dissatisfaction without directly addressing the issue.
- Avoiding responsibility: If I want to avoid taking responsibility for my actions or decisions, I may use passive-aggressive tactics to deflect blame, make others feel guilty, or manipulate situations to avoid accountability.



