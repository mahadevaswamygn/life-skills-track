## 1. Tiny Habits - BJ Fogg

# Your takeaways from the video (Minimum 5 points)
- Changing your environment can influence your behavior, leading to changes in your habits and actions.
- Behavior change is not solely reliant on motivation and is more effectively achieved through additional strategies and support systems.
- To successfully change behavior, it is important to try new approaches, reflect on their effectiveness, and be adaptable in modifying your strategies.
- Instead of solely fixating on desired outcomes, direct your focus towards modifying and improving the specific behaviors that lead to those outcomes.
- By actively changing your behavior, you gain insights and a deeper understanding of how behavior operates and the factors that influence it.

## 2. Tiny Habits by BJ Fogg - Core Message

# Your takeaways from the video in as much detail as possible
### Universal Formula For Human Behavior
 Behavior = motivation+ability+prompt

Tiny habits method involves three parts
##### Shrink the Behavior
Shrink every new habit to tiny possible version so that very little motivation is needed to do it.

##### Identify an active prompt
###### There are three types of prompt
- External prompt(includes alarm,reminder etc)
- Internal prompt(includes thoughts and emotions that remind you to act)
- Active prompt(completion of one behavior remind you to start next behavior)

##### Grow your habit with some shine
If you continue to nurture your habit with some shine it will naturally grow in to something huge.
Learn to celebrate after a tiny win is most critical component of habit development.

# How can you use B = MAP to make making new habits easier?
- Break the new habit into small, easy actions that require minimal motivation to accomplish.
- Make the behavior more enjoyable or rewarding, celebrate small successes, and acknowledge progress to stay motivated.
- Simplify the behavior to make it easier to do by removing obstacles and using reminders or prompts to aid in remembering and practicing the habit.

# Why it is important to "Shine" or Celebrate after each successful completion of habit?
- By celebrating after successfully completing a habit, it reinforces positive behavior.
- By providing a sense of accomplishment and satisfaction, it boosts motivation.
- By celebrating, you build momentum and make it easier to keep practicing the habit

## 3. 1% Better Every Day Video

# Your takeaways from the video (Minimum 5 points)
- Habits are powerful as they gradually build up over time, resulting in meaningful self-improvement.
- See mistakes as chances to learn and remain resilient in getting back on track.
- To build good habits, keep it simple, be consistent, and use rewards to reinforce positive behavior.
- Tailoring habits to your preferences and trying different approaches can lead to improved outcomes.
- Being part of a supportive community boosts motivation and accountability for forming habits.

## 4. Book Summary of Atomic Habits

# Write about the book's perspective on habit formation from the lens of Identity, processes and outcomes?
- To change habits, adopt a new identity that aligns with your aspirations.
- Create effective systems that support desired habits using strategic processes and habit stacks.
- Achieve significant long-term progress by focusing on consistent small improvements.
- Master the habit loop by understanding and leveraging cues, cravings, responses, and rewards for establishing and modifying habits.
- Achieve sustained growth and long-term success by prioritizing daily habits that create a positive feedback loop for continuous improvement.

# Write about the book's perspective on how to make a good habit easier?
- Set up your environment to facilitate good habits.
- Incorporate new habits into your existing routines through habit stacking.
- Establish a clear plan for executing habits through implementation intentions.
- Utilize effective cues to prompt desired behaviors through cue optimization.
- Reduce friction to make positive habits easier and negative habits more challenging.

# Write about the book's perspective on making a bad habit more difficult?
- Make it more challenging to engage in bad habits by adding obstacles.
- Modify your environment by rearranging it to eliminate triggers for the bad habit.
- Disrupt cues to weaken the association with the habit, making it harder to engage in.
- Reduce the appeal of the undesirable habit by pairing it with a desirable one through temptation bundling.
- Enhance awareness of negative outcomes by making consequences visible through visual reminders or journaling.
- Increase awareness of negative outcomes by visually reminding yourself or journaling about the consequences.

## 5. Reflection

# Pick one habit that you would like to do more of? What are the steps that you can take to make it make the cue obvious or the habit more attractive or easy and or response satisfying?
### Reading Books

I fallow this steps to make my  habit more attractive, easy, and satisfying.

1. I will Set a reading goal.
2. Create a reading routine.
3. I choose books that I'm interested in.
4. I will start with shorter reading sessions.
5. I will set up a conducive reading environment for effective studying.
6. I track my progress.

# Pick one habit that you would like to eliminate or do less of? What are the steps that you can take to make it make the cue invisible or the process unattractive or hard or the response unsatisfying?
### Excessive Social Media Usage.

I follow these steps to make a habit less appealing, challenging, and unsatisfying.

1. Limit access.
2. I disable notifications.
3. I will set designated times to check and use social media, enabling better control over time management and preventing mindless scrolling.
4. "Find alternatives to social media by engaging in fulfilling activities such as reading, exercising, pursuing hobbies, or spending quality time with loved ones, making the allure of social media less appealing.
5. Reflect on the negative impacts from social media.









