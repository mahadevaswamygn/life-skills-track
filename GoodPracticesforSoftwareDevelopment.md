# 1. What is your one major takeaway from each one of the 6 sections. So 6 points in total.
- When gathering requirements, it is crucial to identify assumptions and understand the necessary conditions to ensure a successful project.
- During meetings, it is recommended to keep your video turned on. This helps build a better connection with your team and enhances communication.
- Allocating time for your company, the product you're working on, and your team members is essential for enhancing communication within the team and benefiting overall productivity.
- Being responsive and available when someone responds to your message is crucial for expediting progress. Real-time conversations enable faster communication compared to prolonged back-and-forth exchanges, resulting in increased efficiency.
- Maintaining your phone on silent mode, removing notifications from the home screen, and limiting notifications to work-related apps can help minimize distractions and enhance focus on important tasks.
- Understanding your strengths and weaknesses is a crucial takeaway from getting to know your teammates. This knowledge allows for better collaboration and allocation of tasks based on individual skills and abilities.

# 2. Which area do you think you need to improve on? What are your ideas to make progress in that area?
- Giving strong attention to the goal is vital for staying focused and ensuring that efforts and actions align with the desired outcome. It helps maintain clarity and direction throughout the project or task.
- As a software developer, it is important to continuously work on improving your skills and expertise in the field. This involves staying updated with the latest technologies, learning new programming languages, practicing coding exercises, and actively seeking opportunities for professional growth.
- Developers are naturally curious individuals who have a genuine love for learning. They enjoy exploring new technologies and approaches, which fuels their growth and success in the field.
