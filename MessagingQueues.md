## What are messaging queues?
Messaging queues are like virtual mailboxes for computer programs. Instead of programs talking to each other directly, they send messages to a queue. The queue holds onto these messages until the intended programs are ready to receive and process them.


## Why they are used?
Messaging queues are used for various reasons in software systems. Here are some common use cases and benefits of using messaging queues:
- Guaranteed Message Delivery: Once the message is placed in a message queue, it will not be lost until it is consumed by the recipient.
- Asynchronous Communication: Messaging queues allow communication between components or systems without needing them to be connected or available at the same time. This flexibility enables efficient and independent processing of messages.
- Scalability and Load Balancing: Messaging queues help share the workload among multiple consumers, so different applications or multiple instances of the same application can work together to process messages efficiently.


## What are popular tools?
There are several popular messaging queue tools and technologies available:
- RabbitMQ: RabbitMQ is a powerful and flexible messaging queue system that is open-source. It supports different messaging protocols, such as AMQP,MQTT etc.
- Apache Kafka: Apache Kafka is a powerful platform for handling fast and continuous data streams.
- Amazon Simple Queue Service (SQS): It manages the queues for messages, making sure they are reliable and can handle a large number of messages.
- Microsoft Azure Service Bus: It allows for sending and receiving messages, supports different ways of messaging like publishing and subscribing, and offers both push and pull models. 
- Google Cloud Pub/Sub: It allows applications and services to communicate with each other in a flexible and reliable way.

## What is Enterprise Message Bus?
An enterprise message bus is like a special messenger for computer programs. It helps them talk to each other and share information in the right order.

The message bus is like a language translator for computer programs. It helps them understand each other even if they speak different languages or have different rules. This way, the programs can work together and share information without needing to know all the complicated technical stuff about each other.

The message bus is like a friendly middleman that helps programs communicate and work together smoothly, just like how a messenger helps people talk to each other.

## Resources
- chatGpt.
- [blog](https://medium.com/must-know-computer-science/system-design-message-queues-245612428a22)



