# Paraphrase (summarize) the video in a few lines. Use your own words.
- Grit is having both passion and perseverance to achieve long-term goals.
- Grit means not giving up on your goal, even when things get tough, you make mistakes, or progress is slow.- Grit is a better predictor of success in school and life than being a quick or intelligent learner.
- In school and life, having grit is a stronger predictor of success than simply being quick or intelligent.

# What are your key takeaways from the video to take action on?
- Believing that my ability to learn is not fixed and can improve with effort.
- Grit is not a tiny-term goal you need to have a long-term goal with passion, focused even in difficult times.
# Paraphrase (summarize) the video in a few lines in your own words.
- A growth mindset truly creates a solid foundation for great learning.
- It is a powerful idea for driving growth.- There are two ways to think about learning
1. Fixed mindset.
2. Growth mindset.

## Fixed mindset
- They believe that skills and intelligence are fixed and that people are inherently good or not good at certain things.- They believe that you have no control over your abilities.
## Growth mindset
- They believe that skills and intelligence can be grown and developed.- They believe that you are in control of your abilities.

## There are four ingredients to growth
Two mindset people think about these four ingredients in different ways.
1. Effort.
2. Challenges.
3. Mistakes.
4. Feedback.

# What are your key takeaways from the video to take action on?
- Embrace challenges: View challenges as opportunities for growth rather than obstacles. Embracing challenges helps me to develop new skills and knowledge.
- Learn from failures: Instead of being discouraged by failures, I will strive to correct them in my life.
- Increase effort: I will put more effort into succeeding.- Feedback: Take feedback as valuable input for growth.

# What is the Internal Locus of Control? What is the key point in the video?
- having a high Internal locus of control believing that they can control their success or failures but it's not the case everywhere because success or failures in not the result of luck or chance. so in reality having a high locus of control is to not attribute success or failure to factors outside of their control. 

# Paraphrase (summarize) the video in a few lines in your own words.
- having a fixed mindset is not good for anyone at any point of time in our life.- having a growth mindset allows us to grow and develop in each stage of our life.

# What are your key takeaways from the video to take action on?
1. Believe in your ability to figure things out.
2. Question your assumptions.
3. Create your curriculum for growth.
4. Honor the struggle.

# What are one or more points that you want to take action on from the manual? 
- I will take ownership of the projects assigned to me. Its execution, delivery, and functionality are my sole responsibility.
- I will wear confidence in my body. I will stand tall and sit straight.
- I will follow the steps required to solve problems:
RelaxFocus - What is the problem? What do I need to know to solve it?
Understand - Documentation, Google, Stack Overflow, Github Issues, InternetCodeRepeat




















