## What kinds of behaviour cause sexual harassment?
3 forms of sexual harassment

### 1. verbal harassment:
- comments about clothing.
- about a person's body.
- gender-based jokes.

### 2. Visual harassment :
- drawing/picture
- emails or texts.
- screen saves.

### 3. Physical harassment :
- sexual assault.
- impeding or blocking movement.
- inappropriate touching.
- sexual gesturing.

## What would you do in case you face or witness any incident or repeated incidents of such behaviour? 
- If I feel in immediate danger, get away from the situation and ask for help.
- Record the situation if it's possible.
- I am asking for help from friends.
- If this kind happens in the workplace I will inform our manager.
- I will call the police.

